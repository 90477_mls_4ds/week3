# Week3 - 24-26 November 2021

Day | Lesson
---|---
24 November | Solution Exercise - Lesson5 (see week2/exercises)
24 November | [Projects available](./slides/Projects_available.pdf)
24 November | [California housing price ML applications](./scripts/california_housing_price_ml_app.ipynb)
25 November | [California housing price ML applications](./scripts/california_housing_price_ml_app.ipynb)
25 November | [Working with RE](./scripts/regular_expressions.ipynb)
25 November | [Working with strings](./scripts/working_with_strings.ipynb)
26 November | [MNIST digit prediction](./scripts/mnist.ipynb)
26 November | [Predict survival on TITANIC](./scripts/titanic_classifier.ipynb)

NOTE:
* Solution Exercise - Lesson6 (see week2/exercises)
